import axios from "axios";

const baseUrl = process.env.REACT_APP_API_URL;

export const request = async (endpoint, data, method = 'GET', headers) => {
    try {
        const url = `${baseUrl}/${endpoint}`;
        switch (method) {
            case 'GET':
                const response = await axios.get(url, {
                    headers: {
                        'Content-Type': 'application/json',
                        ...headers,
                    },
                    params: data,
                });
                return {
                    status: response.status,
                    data: response.data,
                }

            case 'POST':
                const response = await axios.post(url, data, {
                    headers: {
                        'Content-Type': 'application/json',
                        ...headers,
                    }
                });
                return {
                    status: response.status,
                    data: response.data,
                }

            case 'PUT':
                const response = await axios.put(url, data, {
                    headers: {
                        'Content-Type': 'application/json',
                        ...headers,
                    }
                });
                return {
                    status: response.status,
                    data: response.data,
                }
            
            case 'DELETE':
                const response = await axios.delete(url, {
                    headers: {
                        'Content-Type': 'application/json',
                        ...headers,
                    },
                    params: data,
                });
                return {
                    status: response.status,
                    data: response.data,
                }

        }

    } catch ({ response: { status, data } }) {
        return {
            status,
            data,
        }
    }
}