## Como arrancar

1. Abrir una consola que usaremos para el cliente

2. Correr lo siguientes comandos
```bash
cd client
yarn install
yarn start
```

3. Abrir otra consola que usaremos para el servidor, sin cerrar la anterior

4. Correr lo siguientes comandos
```bash
cd server
npm install
nodemon
```

Nota: si no tenés `nodemon` instalado, poné el comando `npm i -g nodemon` y volver a intentarlo

## Qué hacer?

Hacer el todo list, agregando todo lo necesario en react y completando la lógica para hacer el CRUD (create, read, update, delete) de los TODOs en el servidor.

Podés ayudarte de `http.js` para hacer la consulta al servidor desde el cliente

1. En el cliente, mostrar el todo-list. Probar con Talend API Tester que datos trae

2. En el servidor, poder añadir todo (Método POST) dentro de app.js. Verificar con Talend API Tester si está bien hecho

3. En el cliente, poder añadir todo

4. En el servidor, poder eliminar todo (Método DELETE) dentro de app.js. Verificar con Talend API Tester si está bien hecho

5. En el cliente, poder eliminar todo (por ejemplo, tocando un botón)

6. En el servidor, poder actualizar todo (Método PUT) dentro de app.js. Verificar con Talend API Tester si está bien hecho

7. En el cliente, poder actualizar si el todo está completado (por ejemplo, tocando un botón)
