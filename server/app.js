const express = require('express');
const bodyParser = require("body-parser");
const cors = require("cors");
const uuid = require("uuid");

const app = express();

app.use(cors());
app.use(express.json());
app.use(bodyParser.json());

const list = [
    {
        id: uuid.v4(),
        title: "Learn React",
        completed: false,
    }
];

app.get("/", (req, res) => {
    res.status(200).json({
        entries: list,
    })
});

app.post("/", (req, res) => {
    const id = uuid.v4();
    const { title, completed } = req.body;

    const todo = {
        //COMPLETAR
    };
    //GUARDAR todo EN list

    res.status(201).json({
        todo,
        message: "Todo añadido",
    });
});

app.put("/:id", (req, res) => {
    const { id } = req.params;
    const { completed } = req.body;

    //ACTUALIZAR el campo 'completed' del todo EN list
    const todo = {} //esta linea podes borrarla o no, como quieras. 
    // Pero hay que devolver el todo actualizado en el json

    res.status(200).json({
        todo,
        message: "Todo actualizado",
    });
});

app.delete("/:id", (req, res) => {
    const { id } = req.params;

    //BORRAR todo EN list
    //USAR findIndex y splice para borrar el todo
    const todo = {} //esta linea podes borrarla o no, como quieras. 
    // Pero hay que devolver el todo eliminado en el json

    res.status(200).json({
        todo,
        message: "Todo borrado",
    });
});

app.listen(8080, () => {
    console.log(`The application is listening on port ${8080}!`);
})

